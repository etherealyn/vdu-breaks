<?php
    $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
    // TODO: add support for russian language
?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="data/img/favicon.ico" type="image/x-icon" />
    <title>VDU Helper</title>

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
    <link rel="stylesheet" href="data/css/style.css" />
</head>
<body onload="initialize()">
    <div class="container-fluid">
        <h1><abbr title="Visual Display Unit">VDU</abbr> Helper <small> <br>Track the Amount of Time You've Spent on Anything That Has a Display<span class="asterisk" data-toggle="tooltip" title="...with a web-browser and JavaScript support">*</span></small></h1>
        <hr>
        <p>How often do you take breaks working on your PC, Laptop, Smartphone (or whatever has a display)?</p>
        <p>The <a href="http://www.hse.gov.uk/contact/faqs/vdubreaks.htm" target="_blank">recommended</a> amount of time varies for everyone.</p>
        <p>However, you <strong>should</strong> take a break every 5-10 minutes for 50-60 minutes of work:</p>
        <blockquote>Short, frequent breaks are more satisfactory than occasional, longer breaks:
            e.g., a 5-10 minute break after 50-60 minutes continuous screen and/or keyboard work is likely to be better
            than a 15 minute break every 2 hours
            <footer>from <a href="http://www.hse.gov.uk/contact/faqs/vdubreaks.htm" target="_blank">the recommended</a> page</footer>
        </blockquote>

        <hr>
        <div class="jumbotron" style="text-align: center"><h1 id="timer"></h1>

            <button type="button" class="btn btn-default btn-sm" onclick="restartTimer()">
                <span class="glyphicon glyphicon-repeat"></span> Restart
            </button>
        </div>

        <p class="text-warning"><span class="glyphicon glyphicon-warning-sign"></span> Tips:</p>
        <ol>
            <li><span class="glyphicon glyphicon-eye-open"></span> Leave this page open while you work. It will notify you when 50-60 minutes are gone.</li>
            <li><span class="glyphicon glyphicon-bullhorn"></span> <span id="browser-notifications" onclick="enableNotifications()"></span></li>
            <li><span class="glyphicon glyphicon-bookmark"></span> Set this page as your Home Page or bookmark it</li>
            <li><a href="mailto:satybaldiev_a@auca.kg"><span class="glyphicon glyphicon-envelope"></span> Request a feature:</a> satybaldiev_a@auca.kg
            </li>
        </ol>

    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="src/js/main.js"></script>
</body>
</html>