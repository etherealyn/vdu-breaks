var elapsedSeconds;
var timerHandler;
var maxMinutes;

var title = "Take a break";
var advices = [ 'take a shower', 'drink a cup of tea', 'make a sandwich', 'do some yoga', 'do some meditation' ];

function notifyUser() {
    if ('Notification' in window) {
        var randomIndex = Math.floor((Math.random() * advices.length));
        var randomAdvice = advices[randomIndex];

        var options = {
            body: "You've (probably) been staring at your screen for " + maxMinutes + " minutes. Go "
            + randomAdvice + ". Click [Restart] once you return"
        };

        Notification.requestPermission(function() {
           new Notification(title, options);
        });

        document.title = "Break -> Restart";
    }
}

function updateTimer() {
    elapsedSeconds += 1;

    var minutes = parseInt(elapsedSeconds / 60);
    var hours = parseInt(minutes / 60);

    setTimerText(hours % 24, minutes % 60, elapsedSeconds % 60);

    if (minutes >= maxMinutes) {
        notifyUser();
        pauseTimer();
    }
}

function checkTime(i) {
    if (i < 10) {
        i = "0" + i
    }  // add zero in front of numbers < 10
    return i;
}

function startTimer() {
    elapsedSeconds = 0;

    maxMinutes = getRandomInt(50, 60);
    console.log(maxMinutes);

    timerHandler = setInterval(function() { updateTimer() }, 1000);
    setTimerText(0, 0, 0);
}

function restartTimer() {
    clearInterval(timerHandler);
    startTimer();
}

function pauseTimer() {
    clearInterval(timerHandler);
}

function setTimerText(h, m, s) {
    h = checkTime(h);
    m = checkTime(m);
    s = checkTime(s);

    document.getElementById('timer').innerHTML = h + ":" + m + ":" + s;
    document.title = (h != '00' ? h : '') + m + ":" + s;
}

function getRandomInt (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();

    if ('Notification' in window) {
        setNotificationInfo();
        Notification.requestPermission(function() {
            setNotificationInfo();
        });
    } else {
        document.getElementById('browser-notifications').innerHTML = "Notifications are not supported by this browser.";
    }
});

function initialize() {
    startTimer();
}

function setNotificationInfo() {
    var notificationInfo = document.getElementById('browser-notifications');

    if (Notification.permission === "granted") {
        notificationInfo.innerHTML = "Notifications are enabled. OK.";
        notificationInfo.className = "bg-success";
    } else if (Notification.permission === "default") {
        notificationInfo.innerHTML = 'Notifications are disabled by default. Click here to enable.';
        notificationInfo.className = "bg-warning";
    } else if (Notification.permission === "denied") {
        notificationInfo.innerHTML = 'Notifications are disabled by you.';
        notificationInfo.className = "bg-danger";
    }
}

function enableNotifications() {
    if ('Notification' in window) {
        Notification.requestPermission(function() {
            setNotificationInfo();
        });
    }
}